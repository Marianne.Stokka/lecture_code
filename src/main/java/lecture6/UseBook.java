package lecture6;

public class UseBook {

	public static void main(String[] args) {
		//make 2 new books
		Book inf102Book = new Book("Robert",376,"Algorithms");
		System.out.println("Author: "+inf102Book.getFirstAuthor());
		Book myThesis = new Book("Martin", 210, "New width parameters");
		System.out.println("Author: "+myThesis.getFirstAuthor());

		//myFavorite will now reference to same object as myThesis
		Book myFavorite = myThesis;
		System.out.println("Pages favorite: "+myFavorite.numPages);

		//== will say true since they reference to same object
		System.out.println("use == "+(myFavorite == myThesis));
		//make a new object equal to myThesis
		Book myThesis2 = new Book("Martin", 210, "New width parameters");
		//these two objects are equal but not the same object
		System.out.println("use == "+(myThesis == myThesis2));
		//if the equals method is not changed it will return false
		//if we want to make this return true we need to implement the equals method
		System.out.println("use .equals "+(myThesis.equals(myThesis2)));

		//when tearing out a page from myThesis the page number changes
		//we say that myThesis changes state
		System.out.println("Pages: "+myThesis.numPages);
		myThesis.tearOutPage();
		System.out.println("Pages: "+myThesis.numPages);
		//since inf102 Book is a different object the page number will no change here
		System.out.println("Pages: "+inf102Book.numPages);
		//since myFavorite is referring to the same object as myThesis
		//the number of pages will change in this variable as well
		System.out.println("Pages favorite: "+myFavorite.numPages);

		//Using System.out.println() to print an object only
		//works if you have implemented the toString method
		//if you have not the default printout contains the memory address
		String textRepresentation = inf102Book.toString();
		System.out.println(textRepresentation);
		System.out.println(myThesis);
	}

}
