package lecture7;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * This class shall store a Quiz with simple arithmetic expressions.
 * First of all this Quiz shall be possible to print and give to students.
 * Solution should also be possible to print to make grading easy.
 * 
 * @author Martin Vatshelle
 */
//TODO: fix this lovely mix of Norwegian and English in class name
public class MatteQuiz {

	//field variable to store a list of questions
	ArrayList<Question> questions;
	
	//Constructor creates an empty Quiz
	public MatteQuiz() {
		questions = new ArrayList<Question>();
	}
	
	//main method that can be run to see how a sample Quiz looks like.
	//Ideally I would have moved this to a seperate file called UseMathQuiz
	public static void main(String[] args) {
		MatteQuiz quiz = new MatteQuiz();
		quiz.generateAddition(5);
		quiz.generateSubtraction(5);
		quiz.generateModulo(5);
		quiz.generateMultiplication(5);
		quiz.printQuestions();
		quiz.printSolutions();
		
		//quiz.take();
	}

	/**
	 * Prints all questions with solutions to simplify grading
	 */
	private void printSolutions() {
		System.out.println("Solutions:");
		for(Question q : questions) {
			System.out.println(q.getQuestionWithSolution());
		}
	}

	/**
	 * Prints all questions to the screen such that they are ready to be distributed.
	 */
	private void printQuestions() {
		System.out.println("Questions:");
		for(Question q : questions) {
			System.out.println(q.getQuestion());
		}
		
	}

	public void take() {
		Scanner sc = new Scanner(System.in);
		int score = 0;
		for(Question q : questions) {
			System.out.println(q.getQuestion());
			int ans = sc.nextInt();
			if(ans == Integer.parseInt(q.getSolution())) {
				System.out.print("Correct. ");
				score ++;
			}
			else {
				System.out.print("Wrong. ");
			}
			System.out.println("You have "+score+"/"+questions.size()+" points");
		}
		
	}

	//******* Generation of Questions *******
	
	
	/**
	 * Generates Questions using the - (minus) operator
	 * @param n number of questions to generate
	 */
	private void generateSubtraction(int n) {
		//TODO: This method has lots of code in common with generateAddition
		//we should solve this duplication by calling the generateQuestion() method
		// num1 - num2 = ans
		Random rand = new Random();
		for(int i=0; i<n; i++) {
		
			int num1 = rand.nextInt(100);
			int num2 = rand.nextInt(100);
			int ans = num1 - num2;
		
			Question q = new Question(num1+" - "+num2+" =",""+ans);
			questions.add(q);
		}	
	}

	/**
	 * Generates Questions using the + (plus) operator
	 * @param n number of questions to generate
	 */
	private void generateAddition(int n) {
		// num1 + num2 = ans
		Random rand = new Random();
		for(int i=0; i<n; i++) {
		
			int num1 = rand.nextInt(100);
			int num2 = rand.nextInt(100);
			int ans = num1 + num2;
		
			Question q = new Question(num1+" + "+num2+" =",""+ans);
			questions.add(q);
		}
	}
	
	/**
	 * Generates Questions using an operator given as input
	 * @param n number of questions to generate
	 */
	private void generateQuestion(int n, Operator operator) {
		// num1 - num2 = ans
		Random rand = new Random();
		for(int i=0; i<n; i++) {
		
			int num1 = rand.nextInt(100);
			int num2 = rand.nextInt(100);
			int ans = operator.doOperaion(num1, num2);
		
			Question q = new Question(num1+" "+operator.getOperator()+" "+num2+" =",""+ans);
			questions.add(q);
		}	
	}
	
	/**
	 * Generates Questions using the * (multiply) operator
	 * @param n number of questions to generate
	 */
	private void generateMultiplication(int n) {
		generateQuestion(n, new Multiplication());
	}

	/**
	 * Generates Questions using the % (modulo) operator
	 * @param n number of questions to generate
	 */
	private void generateModulo(int n) {
		generateQuestion(n, new Modulo());
	}



}
