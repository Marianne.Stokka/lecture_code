package lecture7;

public class UseEquals {

	public static void main(String[] args) {
		String navn = "Martin";
		String foreleser = "Martin";
		System.out.println("Strings are same objects "+ (navn == foreleser));
		
		foreleser = new String("Martin");
		System.out.println("Strings using new are same objects "+ (navn == foreleser));
		System.out.println("Strings using new are equal "+ (navn.equals(foreleser)));
		
		Integer num1 = 240;
		Integer num2 = 240;
		System.out.println("Integers are same objects "+(num1 == num2));
	}

}
